var data = [
  {
    name: "Aceituno Oliver, Ángel",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Adán Uria, Rafael",
    area: "Comercial Educacion Delegación Centro"
  },
  {
    name: "Adrio Alías, Mónica",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Alaejos Casado, María Pilar",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Albert Bernal, Carlos",
    area: "Dirección Corporativa Estrategia"
  },
  {
    name: "Alcalde Díaz, Raquel",
    area: "Edición Religiosa"
  },
  {
    name: "Alcalde Moraño Portillo, Montserrat",
    area: "Dirección Producto Tecnológico"
  },
  {
    name: "Alfaro Abreus, Yisel",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Almansa Sanz, Jesús Raúl",
    area: "Asesoría Jurídica"
  },
  {
    name: "Almarza Acedo, María Nieves",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Almisas Salas, Alejandro",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Almohalla Vallejo, José Antonio",
    area: "Edición Religiosa"
  },
  {
    name: "Alonso Amo, Noelia",
    area: "Tesorería"
  },
  {
    name: "Alonso Cruz, Fernando",
    area: "Dirección Sistemas España"
  },
  {
    name: "Alonso Díez de Salazar, Agurtzane",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Alonso Morgado, César",
    area: "Comercial Educación Delegación Mediterranea"
  },
  {
    name: "Alonso Muñoz, María Nieves",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Alonso Negrín, Rosaura",
    area: "Comercial Educación Delegación Canarias"
  },
  {
    name: "Alonso Páez, Begoña",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Alonso Rivero, Daniel",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "Alonso Sánchez, Irene",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "Alonso Suárez, Sergio",
    area: "Logística"
  },
  {
    name: "Alonso Alfaro Vigaray, Pablo",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Álvarez Calabozo, María",
    area: "Comercial Educación Delegación Mediterránea"
  },
  {
    name: "Álvarez Fernández, Carmen María",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Álvarez González, María Carmen",
    area: "Gabinete Dirección General"
  },
  {
    name: "Álvarez González, Natalia",
    area: "Marketing España"
  },
  {
    name: "Álvarez Pedroso, María",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Álvarez Sánchez, Noelia María",
    area: "Fundación SM"
  },
  {
    name: "Álvarez Blázquez Márquez, Blanca",
    area: "Dirección Corporativa Gestión"
  },
  {
    name: "Ameijeiras Vázquez, Francisco",
    area: "Ventas Literatura Infantil y Juvenil "
  },
  {
    name: "Amigo Castro, Serafín",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Amor Martín, Alberto",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Ancochea Toscano, Ana",
    area: "Direccion Corporativa Educación"
  },
  {
    name: "Anglada Pastor, Rocio",
    area: "Tesorería"
  },
  {
    name: "Antón Romero, Javier",
    area: "Dirección Corporativa Gestión"
  },
  {
    name: "Aparicio Pérez, Javier",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Aragón Sánchez, Mario",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Aranda Aranda, María Isabel",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Arévalo Garbayo, Josefina",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Arias Alvarado, Paola del Valle",
    area: "SM Formación"
  },
  {
    name: "Arias Figuerola Ferretti, Marta",
    area: "Fundación SM"
  },
  {
    name: "Arias Hurtado, Carolina",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Arias Ruiz, Melisa",
    area: "CIAC"
  },
  {
    name: "Arriero Crespo, Lázara María",
    area: "Tesorería"
  },
  {
    name: "Arróspide Plá, María",
    area: "SM Formación"
  },
  {
    name: "Asensio Lance, José Manuel",
    area: "Logística"
  },
  {
    name: "Auberson Peiró, Carolina",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Ayuso Triguero, María Victoria",
    area: "Dirección Financiera España"
  },
  {
    name: "Badillo Braza, Francisco",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Bailón Revuelta San Juan, Sandra",
    area: "Dirección Financiera España"
  },
  {
    name: "Balaña Castany, Cristina",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Balzaretti, María Carla",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Baquero Artigao, Gregorio",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Barrado Fernández, Pedro",
    area: "Edición Religiosa"
  },
  {
    name: "Barreiro García, Miguel Ángel",
    area: "CIAC"
  },
  {
    name: "Barrientos González, Rafael",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Barrios Pérez, María Victoria",
    area: "Dirección Financiera España"
  },
  {
    name: "Barroso Hernández, Manuel",
    area: "CIAC"
  },
  {
    name: "Bartolomé Corral, Laura",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Bascón Canales, Jorge",
    area: "Marketing España"
  },
  {
    name: "Bautista Marugán, José Carlos",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Bayarri Ruiz, Josep",
    area: "Marketing LIJ CruÏlla"
  },
  {
    name: "Béjar Gómez, Eva María",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Bejarano Herrero, Iván",
    area: "Logística"
  },
  {
    name: "Beltrán Aragoneses, José",
    area: "Edición Religiosa"
  },
  {
    name: "Beltrán Bustos, Ana",
    area: "Dirección Financiera España"
  },
  {
    name: "Benito Baltasar, Paula",
    area: "Fundación SM"
  },
  {
    name: "Benito Martín, Violeta",
    area: "D General España"
  },
  {
    name: "Bernabéu Ruiz, Javier",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Berrio Gallego, María Mercedes",
    area: "Dirección Producto Tecnológico"
  },
  {
    name: "Beteta Rivero, Iván",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Bilbao Zurinaga, Amelia",
    area: "Dirección Sistemas España"
  },
  {
    name: "Blanco Alonso, Vanesa",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Blasco Ruiz, Ana",
    area: "Edición Religiosa"
  },
  {
    name: "Bombicini Pintor, Tales",
    area: "Dirección Corporativa Gestión"
  },
  {
    name: "Borrajo de Orozco Arnau, David",
    area: "Comercial Educación Delegación Mediterránea"
  },
  {
    name: "Borrás Rosales, Laura",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Boyer, Claire Julie",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Brandariz Montesinos, Gabriel",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Bravo Cordova, Yovana Adriana",
    area: "Gabinete Dirección General"
  },
  {
    name: "Bravo Corregidor, José María",
    area: "Logística"
  },
  {
    name: "Bravo García, Evaristo",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Bravo García, Félix Antonio",
    area: "DIS"
  },
  {
    name: "Bueno Jiménez, Manuel",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Bustos Palomo, Raúl",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Caballer Garrido, Héctor Bartolomé",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Cacho Ruiz, María Paula",
    area: "Dirección Sistemas España"
  },
  {
    name: "Calderón Gallego, Carmen",
    area: "Dirección Financiera España"
  },
  {
    name: "Calderón Juez, Francisco José",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Cáliz Torrens, Sonia María",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "Calvo Leal, Violeta",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "Camacho Collado, Elena",
    area: "Fundación SM"
  },
  {
    name: "Camacho Simón, Gema",
    area: "Logística"
  },
  {
    name: "Campo Atienza, Inocente",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Cano Cuadrado, Guillermo",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Cano Lorente, Miguel Ángel",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Cano Ortiz, Cristina",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Cantisano Navarrete, María Rosario",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Cañadas Alvarado, Judit",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Cañamares Ullán, Daniel",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Cañizares Alba, Susana",
    area: "CIAC"
  },
  {
    name: "Caramés Caamaño, Jesús",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Carballo Martín, Milagros",
    area: "Logística"
  },
  {
    name: "Carmona Adorna, Manuel Alejandro",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Carneros Lorenzo, Belén",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Carrasco Burguete, María Carmen",
    area: "Comercial Educación  Delegación Levante"
  },
  {
    name: "Carretero Gutiérrez, María José",
    area: "Dirección Producto Tecnológico"
  },
  {
    name: "Carretero Rodríguez, Felipe",
    area: "Logística"
  },
  {
    name: "Carrión de Lorenzo, Marta",
    area: "D.RRHH.España"
  },
  {
    name: "Carvajal Inestal, Fco José",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Casado Chacón, Ana María",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Casado Crespo, Carlos",
    area: "Dirección Financiera España"
  },
  {
    name: "Casado Mantecón, Laura",
    area: "D.RRHH.España"
  },
  {
    name: "Casado Márquez, José",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Casaus Armentano, Mireia",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Castellano Darias, Gladys",
    area: "Comercial Educación Delegación Canarias"
  },
  {
    name: "Castellano Pozo, Alba",
    area: "Contenidos Educamos"
  },
  {
    name: "Castilla Olivares, Enrique",
    area: "Edición Religiosa"
  },
  {
    name: "Castro Martín, Ana María",
    area: "Dirección Sistemas España"
  },
  {
    name: "Ceballos Recio, María Jesús",
    area: "Logística"
  },
  {
    name: "Cebrián Moreno, Francisco Antonio",
    area: "Edición Religiosa"
  },
  {
    name: "Cejudo Martínez, Carlos",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Celada Caminero, Jose Luis",
    area: "Edición Religiosa"
  },
  {
    name: "Celma Cercadillo, María",
    area: "Marketing España  Cruïlla"
  },
  {
    name: "Centeno Puig, José Luis",
    area: "Edición Religiosa"
  },
  {
    name: "Chicaiza Portilla, Santiago",
    area: "CIAC"
  },
  {
    name: "Chicano Calderón, Eugenio",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Cid Martínez, Francisco Javier",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Cid Resino, Jessica María",
    area: "Logística"
  },
  {
    name: "Claudio Rodríguez, Ángel",
    area: "DIS"
  },
  {
    name: "Claudio Ruíz, José Miguel",
    area: "Logística"
  },
  {
    name: "Colera Zardoya, Alicia",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Contreras García, Noelia María",
    area: "Marketing España"
  },
  {
    name: "Cornejo Díaz, José Luis",
    area: "Fundación SM"
  },
  {
    name: "Corominas Viñolas, Ariadna",
    area: "Cruïlla"
  },
  {
    name: "Corral Muñoz, Óscar",
    area: "Logística"
  },
  {
    name: "Corrales Álvarez, Carmen",
    area: "Edición Religiosa"
  },
  {
    name: "Correa Sancho, Elena",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Corredera Arana, María",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Corroto Labrador, David",
    area: "DIS"
  },
  {
    name: "Costa Perals, Eduard",
    area: "Cruïlla"
  },
  {
    name: "Costas Comesaña, Carmen Pura",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Costero Bolaños, Jorge Antonio",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Cotera Hernández, María Isabel",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Crespo García, María Jesús",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Cruz Palenzuela, Rubén",
    area: "Edición Religiosa"
  },
  {
    name: "Cruz Pérez, Silvia",
    area: "Dirección Financiera España"
  },
  {
    name: "Cruz Salas, Enrique",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Cuétara García, Cynthia María",
    area: "Marketing España"
  },
  {
    name: "Cuevas Gámez, Diego",
    area: "Marketing España"
  },
  {
    name: "Cupini Carrillo, Carolina",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Cuquerella Romero, Irene",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "De Antonio Rodríguez, Marta",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "de la Iglesia Pérez, Inés",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "de Luz Nadal, Beatriz",
    area: "Contenidos Educamos"
  },
  {
    name: "Del Mul Gutiérrez, Ana Paula",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "del Puerto Almazán, Vicente",
    area: "Comercial Educación  Delegación Centro"
  },
  {
    name: "Del Triunfo Morato, Cristina",
    area: "Contenidos Educamos"
  },
  {
    name: "del Valle Magán, Víctor Ricardo",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Depalma Sinibaldi, Paula Marcela",
    area: "Edición Religiosa"
  },
  {
    name: "Dequel Losa, Mario",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Devesa Revert, José Vicente",
    area: "CIAC"
  },
  {
    name: "Díaz Aberturas, María Carmen",
    area: "CIAC"
  },
  {
    name: "Díaz Enrique, Fernando",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Díaz Fernández, Blanca",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Díaz Marbán, Marlene",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Díaz Movilla, Raúl",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Díaz Plaza Martín Lorente, Elena",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Díez Alonso, Rafael",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Domingo Monje, Susana",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "Domingo Sánchez, Mireia",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Domínguez Culebras, José Luis",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Domínguez Gómez, Marta",
    area: "Edición Religiosa"
  },
  {
    name: "Domínguez Lorenzo, María Ángeles",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Duarte Sáez, José María",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Dueñas Sánchez, Verónica",
    area: "Logística"
  },
  {
    name: "Duque Rivero, Domingo",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Durán Barreira, Juan Diego",
    area: "Contenidos Educamos"
  },
  {
    name: "Echegoyen Sanz, Eduardo",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Echevarría Cabrera, Ana Eva",
    area: "Logística"
  },
  {
    name: "Elías Romero, Diana Carolina",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Escalada Chacón, José Luis",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Espasa Rodríguez, Nuria",
    area: "Marketing España"
  },
  {
    name: "Esteban Díaz Guerra, Miguel",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Esteban Puente, María Victoria",
    area: "Contenidos Educamos"
  },
  {
    name: "Esteban Rodríguez, Engelberto",
    area: "Planificación"
  },
  {
    name: "Estebaranz Polo, Inmaculada",
    area: "CIAC"
  },
  {
    name: "Expósito Iñigo, Noemí",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Fernández Barral, Carlos",
    area: "Dirección Educamos España"
  },
  {
    name: "Fernández Cabello, Alberto",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Fernández Cerrato, Miguel Ángel",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Fernández Cerro, Virginia",
    area: "CIAC"
  },
  {
    name: "Fernández Corral, Patricia",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Fernández Crespo, María Asunción",
    area: "D.RRHH.España"
  },
  {
    name: "Fernández García, Bárbara",
    area: "Contenidos Educamos"
  },
  {
    name: "Fernández González, Mónica",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Fernández Luengo, María Pilar",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Fernández Moreno, Rocío Coral",
    area: "Edición Religiosa"
  },
  {
    name: "Fernández Núñez, Juan",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Fernández Pérez, Fernando",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Fernández Plaza, Nuria",
    area: "Logística"
  },
  {
    name: "Fernández Urbina, Ana Isabel",
    area: "Dirección Financiera España"
  },
  {
    name: "Fernández Viejo, José Antonio",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Ferrandiz Cámara, José Luis",
    area: "Dirección Producto Tecnológico"
  },
  {
    name: "Ferrer Prieto, Jesús Manuel",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Ferrero Álvarez, Juan Carlos",
    area: "Comercial Educación Delegación Mediterránea"
  },
  {
    name: "Ferriols Lancho, Juan Carlos",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Flórez Sastre, María Dolores",
    area: "Cuentas Clave"
  },
  {
    name: "Font Ferré, Nuria",
    area: "Cruïlla"
  },
  {
    name: "Fortuny Llambias, Verónica",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Franco Fernández, Miguel Ángel",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Frutos Peña, Lorena",
    area: "SM Formación"
  },
  {
    name: "Fuente Monroy, Roberto",
    area: "Logística"
  },
  {
    name: "Fuentenebro Martínez, Cristina",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Fuentes Palacios, Luis",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Fuentes Río, Mónica",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Gala Cabezas, Yolanda",
    area: "Logística"
  },
  {
    name: "Galindo Gallego, Luis Antonio",
    area: "Planificación"
  },
  {
    name: "Gallas Martínez, Marta",
    area: "Oficina de Autor"
  },
  {
    name: "Gallego Cámara, Alicia",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Gallego Cañas, Alberto",
    area: "Dirección Financiera España"
  },
  {
    name: "Gallego Marín, Francisco Javier",
    area: "Logística"
  },
  {
    name: "Gálvez Amaro, Aurora",
    area: "CIAC"
  },
  {
    name: "Gangas Almendros, Elena",
    area: "SM Formación"
  },
  {
    name: "García Arroyo, Lucio",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "García Carrera, Ana Maria",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "García Cebrián, Vanesa",
    area: "Logística"
  },
  {
    name: "García Coll, Margarida",
    area: "Cruïlla"
  },
  {
    name: "García del Río, Miguel",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "García Díez, Rafael Pablo",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "García Fernández, Marta",
    area: "CIAC"
  },
  {
    name: "García García, María Carmen",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "García García, Pilar",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "García Gargallo, Alba",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "García González, Catalina",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "García Herrero, Ismael",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "García Higueras, Arturo",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "García Lorenzo, María Ángeles",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "García Maroto, Isabel María",
    area: "CIAC"
  },
  {
    name: "García Merino, Arturo",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "García Navarro, Araceli",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "García Pedrero, Guillermo",
    area: "SM Formación"
  },
  {
    name: "García Pradas, Antonio",
    area: "CIAC"
  },
  {
    name: "García Pulido, Inmaculada",
    area: "CIAC"
  },
  {
    name: "García Reus, Santiago",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "García Saavedra, José Jonay",
    area: "Comercial Educación Delegación Canarias"
  },
  {
    name: "García Sánchez, Cristina",
    area: "SM Formación"
  },
  {
    name: "García Sastre, Laura",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "García Tirado, Diego",
    area: "Dirección Sistemas España"
  },
  {
    name: "García Vázquez, Raúl",
    area: "Marketing España"
  },
  {
    name: "García Caro García, Alejandro",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Garza Valenzuela, Marcos",
    area: "Dirección Nuevos Negocios Digitales"
  },
  {
    name: "Gascón Ríos, María Cruz",
    area: "Dirección Corporativa Tecnología y Negocio Digital"
  },
  {
    name: "Gata Claudio, Manuel",
    area: "Dirección Sistemas España"
  },
  {
    name: "Gijón Morales, Álvaro",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Gil Ardoy, José María",
    area: "Dirección Producto Tecnológico"
  },
  {
    name: "Gil Paredes, Francisco José",
    area: "Dirección Corporativa Gestión"
  },
  {
    name: "Gil Puerto, Eva",
    area: "Logística"
  },
  {
    name: "Giménez Viciana, Herminia",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Gómez Blazquez, Raquel",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Gómez Fernández, María",
    area: "Edición Religiosa"
  },
  {
    name: "Gómez Pérez, Eileen Alcira",
    area: "Dirección Producto Tecnológico"
  },
  {
    name: "Gómez Rio, Lucia",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Gómez Sebastián, Juan Antonio",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Gómez Teixeira, Karla",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Gómez Torres, Javier",
    area: "Dirección Nuevos Negocios Digitales"
  },
  {
    name: "Gómez Vivo, Silvia",
    area: "Comercial Educación Delegación Mediterránea"
  },
  {
    name: "Gómez Lobo González Román, Clara",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Gómez Villar Sara, Manuel",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "González Álvarez, Francisco",
    area: "Dirección Financera España"
  },
  {
    name: "González Balseyro, Lourdes",
    area: "Auditoría Interna"
  },
  {
    name: "González Bermúdez, Juan Antonio",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "González Carrobles, Miguel Ángel",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "González de Pedro, Alicia",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "González Escobar, José Antonio",
    area: "Dirección Comercial Centros"
  },
  {
    name: "González Fernández, Alejandra",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "González Fernández, Francisco Javier",
    area: "Edición Religiosa"
  },
  {
    name: "González Fernández, Pedro José",
    area: "Contenidos Educamos"
  },
  {
    name: "González García, María Pilar",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "González González, Carolina",
    area: "Marketing España"
  },
  {
    name: "González González, Jesús",
    area: "Dirección Financiera España"
  },
  {
    name: "González Jurado, Mario Félix",
    area: "Edición Religiosa"
  },
  {
    name: "González Llopis, Ana",
    area: "Marketing España"
  },
  {
    name: "González López, Ana María Piedad",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "González López, María Pilar",
    area: "CIAC"
  },
  {
    name: "González Mariscal, Miriam",
    area: "Logística"
  },
  {
    name: "González Martínez, José Antonio",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "González Mateos, Aranzazu",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "González Nieto, Silvia",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "González Nuñez, Ana María",
    area: "Dirección Sistemas España"
  },
  {
    name: "González Ochoa, José María",
    area: "Fundación SM"
  },
  {
    name: "González Pena, Ana",
    area: "Logística"
  },
  {
    name: "González Peñalba, Cristina",
    area: "Dirección Producto Tecnológico"
  },
  {
    name: "González Rodríguez, Ana Almudena",
    area: "Dirección Sistemas España"
  },
  {
    name: "González Tejada, Francisco",
    area: "Marketing Educamos"
  },
  {
    name: "González Valenzuela, Maitane",
    area: "Marketing Educamos"
  },
  {
    name: "Gonzalo Sánchez, María José",
    area: "Oficina de Autor"
  },
  {
    name: "Guardia Ferrera, María Beatriz",
    area: "Comercial Educación Delegación Canarias"
  },
  {
    name: "Guerra García, Andrés",
    area: "Fabricación"
  },
  {
    name: "Guillén Pedrero, Joaquín",
    area: "Logística"
  },
  {
    name: "Gumpert Melgosa, Carlos",
    area: "Contenidos Educamos"
  },
  {
    name: "Gutiérrez Campo, Juan Antonio",
    area: "Logística"
  },
  {
    name: "Gutiérrez Fernández, Jonás",
    area: "Contenidos Educamos"
  },
  {
    name: "Gutiérrez Gabriel, Miguel",
    area: "Auditoría Interna"
  },
  {
    name: "Guzmán Campos, Susana",
    area: "Oficina de Autor"
  },
  {
    name: "Heras Fernández, Ana Beatriz",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Hernández Domínguez, Cristina",
    area: "Dirección Corporativa Educación"
  },
  {
    name: "Hernández Hernández, Alicia",
    area: "Dirección Financiera España"
  },
  {
    name: "Hernández Martín, María Sonsoles",
    area: "Edición Religiosa"
  },
  {
    name: "Hernández Matías, Yolanda",
    area: "Contenidos Educamos"
  },
  {
    name: "Hernández Pereda Velasco, Amparo",
    area: "Edición Religiosa"
  },
  {
    name: "Hernández Petisco, María Jesús",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Hernández Romero, Isidoro",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Herranz Palma, Raúl",
    area: "Logística"
  },
  {
    name: "Herrera Castillo, María Carmen",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Hervás Daza, Olalla",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Hidalgo de Matías, Miguel Ángel",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Hidalgo Perea, Gema",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Hidalgo Rey, María Pia",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Hourdille, Pierre",
    area: "Dirección Nuevos Negocios Digitales"
  },
  {
    name: "Inglés López, María Jesús",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Jiménez Blas, Silvia",
    area: "SM Formación"
  },
  {
    name: "Jiménez Fernández, María Aguila",
    area: "Fabricación"
  },
  {
    name: "Jiménez García, Javier Francisco",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Jiménez Montesinos, Olga Lidia",
    area: "CIAC"
  },
  {
    name: "Jiménez Núñez, Guillermo",
    area: "D.RRHH.España"
  },
  {
    name: "Johansen Rodrigalvarez, Diana María",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Jordán Lorente, Victoria",
    area: "Edición Religiosa"
  },
  {
    name: "Juarros Revilla, María Natividad",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Jurkowska, Patrycja Katarzyna",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Karaschewski, Tamara",
    area: "Dirección Corporativa Gestión"
  },
  {
    name: "Krzysztalowska, Agata",
    area: "Direccion Corporativa Educación"
  },
  {
    name: "Laguna Matute, José Miguel",
    area: "Edición Religiosa"
  },
  {
    name: "Laporta Hernández, Laura",
    area: "Contenidos Educamos"
  },
  {
    name: "Lara Hierro, Paloma",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Las Heras Negueruela, Carlos",
    area: "Dirección Comercial Centros"
  },
  {
    name: "Lema Luna, Esther",
    area: "SM Formación"
  },
  {
    name: "Lezcano Estévez, Silvia",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Lidó Micó, Rafael",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Lite López, Jorge",
    area: "Contenidos Educamos"
  },
  {
    name: "Lizano Herrera, María Transito",
    area: "Logística"
  },
  {
    name: "Llistosella Noguera, José",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Llorente Galeán, Enrique",
    area: "Dirección Producto Tecnológico"
  },
  {
    name: "Lobato Villagrá, Pablo",
    area: "Contenidos Educamos"
  },
  {
    name: "López Castellanos, María Dolores",
    area: "Dirección Corporativa Educación"
  },
  {
    name: "López Felipe, Alberto",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "López Gil, Antonio",
    area: "Dirección Corporativa Estrategia"
  },
  {
    name: "López Lucas, Ángela",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "López Martínez, Iván",
    area: "Logística"
  },
  {
    name: "López Ramirez, Dolors",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "López Relaño, Ana",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "López Uriarte, María Reyes",
    area: "SM Formación"
  },
  {
    name: "Lorenzo López, José",
    area: "Edición Religiosa"
  },
  {
    name: "Lorenzo Lorenzo, Elvira Inés",
    area: "Contenidos Educamos"
  },
  {
    name: "Lorenzo Ontiyuelo, Óscar",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Lorrio Alonso, José Luis",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Losada Nestares, Sofía",
    area: "Dirección Corporativa Gestión"
  },
  {
    name: "Lozano Berruguete, Ana Isabel",
    area: "Dirección Financiera España"
  },
  {
    name: "Lozano Berruguete, Olga María",
    area: "Gabinete Dirección General"
  },
  {
    name: "Lozano Gaviño, Maria Carmen",
    area: "Dirección Financiera España"
  },
  {
    name: "Lozano Ramírez Arellano, Yolanda",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Lózar Sáez, Ana María",
    area: "Dirección Sistemas España"
  },
  {
    name: "LUENGO LORENZO, ALMUDENA",
    area: "Dirección Corporativa Gestión"
  },
  {
    name: "Luján García, María Ángeles",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Machuca Vales, Estrella",
    area: "Fundación SM"
  },
  {
    name: "Madre Ramos, Enrique",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Madurga Lozano, Marta",
    area: "Comercial Educación Delegación Mediterránea"
  },
  {
    name: "Malavia Martínez, Miguel Angel",
    area: "Edición Religiosa"
  },
  {
    name: "Manso Ortiz, Inés",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Mañas Martínez, Maria Mar",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Maqueda Gil, Diego",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Marañón Castelló, Jorge",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Marcos Ruiz, Irene",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Mares Gallego, Noemí",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "María Pérez, Nemesia",
    area: "Fabricación"
  },
  {
    name: "Marín Espinosa, María Dolores",
    area: "Comercial Educación Delegación Mediterránea"
  },
  {
    name: "Marín Reneses, Mariano",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Mariscal Paredes, José Antonio",
    area: "Dirección Financiera España"
  },
  {
    name: "Márquez Díaz, Eduardo",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Márquez Molleda, Berta",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Martín Aguirre, Adoración",
    area: "Dirección Financiera España"
  },
  {
    name: "Martín Bermejo, Olga",
    area: "Dirección Educamos España"
  },
  {
    name: "Martín Campo, José Ignacio",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Martín Heredia, María",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Martín Hernández, Nayra Pino",
    area: "Comercial Educación Delegación Canarias"
  },
  {
    name: "Martín Juárez, Benita Jerónima",
    area: "DIS"
  },
  {
    name: "Martín Llamazares, Margarita",
    area: "Dirección Corporativa Educación"
  },
  {
    name: "Martín Ochando, Alejandro",
    area: "Logística"
  },
  {
    name: "Martín Rivals, Patricia",
    area: "Dirección Financiera España"
  },
  {
    name: "Martín Sánchez, Miriam",
    area: "CIAC"
  },
  {
    name: "Martín Vidal, José Ángel",
    area: "Logística"
  },
  {
    name: "Martínez Aniorte, Juan Carlos",
    area: "Contenidos Educamos"
  },
  {
    name: "Martínez Arroyo, Alberto",
    area: "Logística"
  },
  {
    name: "Martínez Ballesteros, Javier",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Martínez Díaz, Beatriz",
    area: "CIAC"
  },
  {
    name: "Martínez Díaz, Javier",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Martínez Gaona, Marta",
    area: "Logística"
  },
  {
    name: "Martínez García, Yolanda",
    area: "Edición Religiosa"
  },
  {
    name: "Martínez Martín, Arturo",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Martínez Morales, Carmen",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Martínez Moya, María Dolores",
    area: "Logística"
  },
  {
    name: "Martínez Oses, Pedro",
    area: "Edición Religiosa"
  },
  {
    name: "Martínez Paz, Belén",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Martínez Pérez, María José",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Martínez Tejero, Emilio",
    area: "Logística"
  },
  {
    name: "Martínez Vallejera, Sergio",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Martín Tembleque Poves, Beatriz",
    area: "CIAC"
  },
  {
    name: "Mas Martínez, Alicia",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Mateo Bermejo, Iván",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Matesanz García, María José",
    area: "Dirección Nuevos Negocios Digitales"
  },
  {
    name: "Mayans Perello, Teresa",
    area: "Dirección Corporativa Educación"
  },
  {
    name: "Mayas Sanz, Rafael",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Medina Camacho, Daniel",
    area: "DIS"
  },
  {
    name: "Medina Domínguez, María del Rocío",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "MEDINA HERNÁNDEZ, GRECIA",
    area: "Edición Religiosa"
  },
  {
    name: "Medina Moreno, Francisca",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Melgosa Nocete, Nuria",
    area: "Dirección Educamos España"
  },
  {
    name: "Membrado Lope, María Pilar",
    area: "Dirección Financiera España"
  },
  {
    name: "Méndez Pérez, Jaime",
    area: "Dirección Financiera España"
  },
  {
    name: "Mercado Roncero, Cecilia",
    area: "Cruïlla"
  },
  {
    name: "Merchán García, María Irene",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Merinero Ortiz, Beatriz",
    area: "Logística"
  },
  {
    name: "Mesa Anguita, Marta",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Miguélez Santos, Carlos",
    area: "Comercial Educación Delegación Mediterránea"
  },
  {
    name: "Modrego Tejada, Rosa María",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Molano Mazón, Julia",
    area: "Dirección Producto Tecnológico"
  },
  {
    name: "Molero Herrero, José Ángel",
    area: "Comercial Educación Delegación Mediterránea"
  },
  {
    name: "Molina Martín, Alejandro",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Molinero Vaquero, Marta",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Monforte Avila, Raquel",
    area: "Dirección Educamos España"
  },
  {
    name: "Montes García, Elisa",
    area: "Dirección Financiera España"
  },
  {
    name: "Montes Pascual, Mónica",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Mora Martínez, Sergio",
    area: "Contenidos Educamos"
  },
  {
    name: "Morales Ávila, Elsy Paulina",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Morales Blázquez, José Manuel",
    area: "Logística"
  },
  {
    name: "Morales Maroto, Óscar",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Morán Ruiz, María Victoria",
    area: "Edición Religiosa"
  },
  {
    name: "Morante Mena, David",
    area: "Logística"
  },
  {
    name: "Moreno Alonso, María",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Moreno Clemente, Eva",
    area: "Logística"
  },
  {
    name: "Moreno Edesa, Ernesto",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Moreno López, Elena",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Moreno Sancho, Isabel María",
    area: "Dirección Corporativa Tecnología y Negocio Digital"
  },
  {
    name: "Moreo Martínez, Ana María",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Moro Cordobés, Inmaculada",
    area: "Dirección Financiera España"
  },
  {
    name: "Moya Martínez, Gema",
    area: "DIS"
  },
  {
    name: "Muiña Merino, Paloma",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Muller Hernández, Esther",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Muñoz Castellanos, Hortensia",
    area: "Edición Religiosa"
  },
  {
    name: "Muñoz Martín, Ignacio",
    area: "D.RRHH.España"
  },
  {
    name: "Muñoz Martínez, María Teresa",
    area: "D.RRHH.España"
  },
  {
    name: "Muñoz Muñoz, Rubén",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Muñoz Oteros, Arturo",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Muñoz Roncero, María Encarnación",
    area: "D.RRHH.España"
  },
  {
    name: "Muñoz Sánchez, Rosa María",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Muñoz Torrealba, José Luis",
    area: "Marketing España"
  },
  {
    name: "Muñoz Urdangaray, Ignacio",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Muñumer Rodríguez, Pedro",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Nacarino Burgos, Eduardo",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Navarro Aldavero, Verónica",
    area: "Contenidos Educamos"
  },
  {
    name: "Navarro Cruz, Pedro Andrés",
    area: "DIS"
  },
  {
    name: "Navarro Hernández, Raquel",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Navarro Marín, Francisco Javier",
    area: "Edición Religiosa"
  },
  {
    name: "NICOLÁS ABENZA, ASCENSIÓN",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Nicolás Álvarez, David",
    area: "Dirección Sistemas España"
  },
  {
    name: "Niebla Chavez, Sandra Leonor",
    area: "Comercial Educación Delegación Canarias"
  },
  {
    name: "Núñez Cataluña, Eduardo",
    area: "Dirección Corporativa Personas"
  },
  {
    name: "Núñez Macías, José Eladio",
    area: "Dirección Financiera España"
  },
  {
    name: "Núñez Pérez, Raul",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Núñez Plaza, Pablo",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "Orcajo Elena, José Carlos",
    area: "Cuentas Clave"
  },
  {
    name: "Ordax Sanz, Gema María",
    area: "Logística"
  },
  {
    name: "Ordóñez Florido, Virginia",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Orozco Villaverde, Daniel",
    area: "Edición Religiosa"
  },
  {
    name: "Ortega Atienza, María Mar",
    area: "Fabricación"
  },
  {
    name: "Ortega Elías, Susana",
    area: "Marketing España"
  },
  {
    name: "Ortiz Perpiñá, Dolors",
    area: "Cruïlla"
  },
  {
    name: "Ortiz Santiago, Juan José",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Ortiz Vélez, María Teresa",
    area: "Dirección Corporativa Educación"
  },
  {
    name: "Oso Carvallo, Macarena",
    area: "Logística"
  },
  {
    name: "Otero Martínez, Herminio",
    area: "Edición Religiosa"
  },
  {
    name: "Palacín Sabando, Javier",
    area: "Dirección Financiera España"
  },
  {
    name: "Palau Sánchez, Carolina",
    area: "Cruïlla"
  },
  {
    name: "Palomino Crespo, María Carmen",
    area: "Dirección Corporativa Estrategia"
  },
  {
    name: "Paredes López, Luis Eduardo",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Parra Navarro, Laura",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Pasamón Lara, Sonia",
    area: "Marketing España"
  },
  {
    name: "Pascual Monge, Óscar",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Pascual Rodríguez, Begoña",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Pastor García, Martín",
    area: "Comercial Educación  Delegación Levante"
  },
  {
    name: "Pauner Escudé, Anna",
    area: "Cruïlla"
  },
  {
    name: "Peces Ruisánchez, Lara",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Pedro Rojo, Antonio",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Pedroso Ortega, Xosé Xabier",
    area: "Edición Religiosa"
  },
  {
    name: "Peiró Guisado, Verónica",
    area: "CIAC"
  },
  {
    name: "Pellicer García Arcicollar, Jaime",
    area: "Edición Religiosa"
  },
  {
    name: "Penanes Blanco, Patricia",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Penilla Ezcurra, Arnaitz",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Peñalver Sánchez, Juan",
    area: "Edición Religiosa"
  },
  {
    name: "Peñaranda Osma, Olga",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Peñín Falagán, Ana María",
    area: "DIS"
  },
  {
    name: "Perea Sánchez, Ángel",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Pereda Sancho, Ana Marta",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Pérez Arnáez, Laura Emilia",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Pérez Bermejo, Javier",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Pérez Carmona, Rubén",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Pérez Casas, Albert",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Pérez Coutado, Ariana",
    area: "Fundación SM"
  },
  {
    name: "Pérez del Río, Iván",
    area: "Edición Religiosa"
  },
  {
    name: "Pérez Gandía, Almudena Silvia",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Pérez García, Juan Luis",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Pérez Garrido, Salvador",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Pérez González, Raquel",
    area: "Dirección Financiera España"
  },
  {
    name: "Pérez Guadalupe, Sergio",
    area: "Comercial Educación Delegación Canarias"
  },
  {
    name: "Pérez Gutiérrez, Carolina",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Pérez Jiménez, Julia",
    area: "DIS"
  },
  {
    name: "Pérez Lambán, Pedro Juan",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Pérez Martínez, Mario",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Pérez Pascual, María Inés",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Pérez Pérez, Ángel",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "PÉREZ PÉREZ, SONIA",
    area: "D.RRHH.España"
  },
  {
    name: "Pérez Rubio, Leopoldo",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Pérez Cejuela García, Gema",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Picó Guzmán, María del Carmen",
    area: "Edición Religiosa"
  },
  {
    name: "Pindado Infante, Cristina",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Pintado Grande, Rosa",
    area: "Marketing España"
  },
  {
    name: "Piñeiro Domínguez, José Ramón",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Piñero Ibáñez, Arnau",
    area: "Comercial Educación Delegación Mediterránea"
  },
  {
    name: "Plaza García, María Isabel",
    area: "CIAC"
  },
  {
    name: "Pons Cañas, Eulalia",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Porras Angoustures, Susana",
    area: "Comercial Educación Delegación Mediterránea"
  },
  {
    name: "Poza Castro, Cristina",
    area: "Dirección Sistemas España"
  },
  {
    name: "Pradas Yuste, María José",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Priego Arrebola, Rafael",
    area: "Fabricación"
  },
  {
    name: "Prieto Melcón, David",
    area: "CIAC"
  },
  {
    name: "Prieto Olmo, José Antonio",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "Prieto Rojo, Enrique",
    area: "Logística"
  },
  {
    name: "Prunera Masip, Cristina",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Puentes Aguilar, María Rosario",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Puerta Flores, Fidel",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Quinoya Blanco, María Siracusa",
    area: "DIS"
  },
  {
    name: "Quiñones López, Silvana",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Rabadán Colomo, Javier",
    area: "Logística"
  },
  {
    name: "Rafael Calderón, Víctor Manuel",
    area: "Logística"
  },
  {
    name: "Rambaud Cabello, Javier",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Ramírez Navarro, Consol",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Real Carretero, Esther",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Rebollo Criado, Antonio David",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Redondo Eugenio, Ana María",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Relaño Villacorta, Luis Jesús",
    area: "Dirección Corporativa Tecnología y Negocio Digital"
  },
  {
    name: "Rey Barrones, Rosa María",
    area: "Comercial Educación Delegación Mediterránea"
  },
  {
    name: "Rey Flores, Mireia",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Ricardo Rodríguez, Natalia Vanina",
    area: "CIAC"
  },
  {
    name: "Rico Sanmartín, María José",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Rioja Gutiérrez, Sara",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "Ríos Villalta, Juan Carlos",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Rivero Moreno, Antonia",
    area: "Edición Religiosa"
  },
  {
    name: "Rocafort Huerta, Juan Antonio",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Rocamora Pérez, Silvia",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Rodrigo Bonachela, Elena",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Rodríguez Bellido, David",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Rodríguez Caballero, Eva",
    area: "Dirección Corporativa Personas"
  },
  {
    name: "Rodríguez Castillo, Raúl",
    area: "Edición Religiosa"
  },
  {
    name: "Rodríguez Díaz, José Manuel",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Rodríguez Dorta, Yamelith Margot",
    area: "Comercial Educación Delegación Canarias"
  },
  {
    name: "Rodríguez Figueroa, José Luis",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Rodríguez Frías, José Ángel",
    area: "Logística"
  },
  {
    name: "Rodríguez González, Mario",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Rodríguez Jiménez, Gemma",
    area: "CIAC"
  },
  {
    name: "Rodríguez Martínez, Olga",
    area: "Dirección Financiera España"
  },
  {
    name: "Rojas García, Marta",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "Roldán Pallarés, José Miguel",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Román Azores, Francisco Javier",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Román López, Beatriz",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Román Monroig, Laura",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Romera Martínez, Alejandro",
    area: "Logística"
  },
  {
    name: "Romero Fernández, María Araceli",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Romero García, Francisco",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Rosal Banegas, Tamara",
    area: "Oficina de Autor"
  },
  {
    name: "Roura Javier, Antonio",
    area: "Edición Religiosa"
  },
  {
    name: "Rozano Alba, Juan Luis",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Rubio Garrido, Ángel Javier",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Ruiz Clemente, Estela",
    area: "Dirección Corporativa Gestión"
  },
  {
    name: "Ruiz Conde, José Alberto",
    area: "Planificación"
  },
  {
    name: "Ruiz Sáez, Julián",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "SÁDABA LURI, IVÁN",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Saiz Sánchez, Raquel",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Sala Sarrión, Aurea",
    area: "CIAC"
  },
  {
    name: "Salas Hernáiz, María Dolores",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Salguero Ramos, Mario",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Sánchez Campal, Roberto",
    area: "Dirección Nuevos Negocios Digitales"
  },
  {
    name: "Sánchez Cristóbal, Javier",
    area: "CIAC"
  },
  {
    name: "Sánchez García Serrano, María",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Sánchez González, José Álvaro",
    area: "Edición Religiosa"
  },
  {
    name: "Sánchez Gutiérrez, Rosa María",
    area: "Logística"
  },
  {
    name: "Sánchez Martín, Beatriz",
    area: "Dirección Corporativa Estrategia"
  },
  {
    name: "Sánchez Muñoz, Susana",
    area: "Dirección Nuevos Negocios Digitales"
  },
  {
    name: "Sánchez Prades, María Antonia",
    area: "Cuentas Clave Cataluña"
  },
  {
    name: "Sánchez Puchol, Carmen",
    area: "Cruïlla"
  },
  {
    name: "Sánchez Salillas, Esther",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Sánchez Sánchez, Julio Javier",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "Sánchez Sánchez, María del Mar",
    area: "Edición Religiosa"
  },
  {
    name: "Sánchez Sierra Pozo, Antonio Emilio",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Sánchez Feijóo Buenaga, Belén",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "Sánchez Ocaña Sans, José Miguel",
    area: "Dirección Sistemas España"
  },
  {
    name: "Sandoval Gómez, Jordi",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Sangrador Rasero, Abilio",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "San Juan Pérez, Esther",
    area: "Logística"
  },
  {
    name: "San Miguel Martos, Julia",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Santana Maldonado, Luis María",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Santos Gordillo, María",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Sanvisens Farrás, Montserrat",
    area: "Cruïlla"
  },
  {
    name: "Sanz Garrido, Ana Blanca",
    area: "Comercial Educación Delegación Centro"
  },
  {
    name: "Sanz Hernán, Jesús Ángel",
    area: "DIS"
  },
  {
    name: "Sarmiento Moscoso, Ana",
    area: "Fundación SM"
  },
  {
    name: "Saturio Álvarez, Rubén",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Schmolk Serrano, Susanne",
    area: "DIS"
  },
  {
    name: "Segarra Monfort, Gemma Carmen",
    area: "Comercial Educación Delegación Mediterránea"
  },
  {
    name: "Segura Cardona, Esther",
    area: "Comercial Educación Delegación Cataluña"
  },
  {
    name: "Sempere Hernández, Jorge Gonzalo",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Serrano Arrogante, María Paz",
    area: "Oficina de Autor"
  },
  {
    name: "Serrano Lorenzo, Eva María",
    area: "Logística"
  },
  {
    name: "Serrano Nicolás, Begoña",
    area: "Comercial Educación Delegación Levante"
  },
  {
    name: "Sierra Vidueira, Sonia",
    area: "Contenidos Educamos"
  },
  {
    name: "Silloniz González, Adolfo",
    area: "Gerencia Relaciones Escuela Católica"
  },
  {
    name: "Sintes, Joanne Liza",
    area: "Marketing España"
  },
  {
    name: "Sochas, Stephanie Marie Pascale Catherin",
    area: "Marketing España"
  },
  {
    name: "Solé Prats, Ferran",
    area: "CIAC"
  },
  {
    name: "Somacarrera Mirón, Laura",
    area: "Dirección Producto Tecnológico"
  },
  {
    name: "Soto González, Sofía Clara",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Sousa Laurenti, Rocío",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Souto Rodríguez, Laura",
    area: "D.RRHH.España"
  },
  {
    name: "Suárez López, Carmen",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Suescun Pérez, Esther",
    area: "Dirección Financiera España"
  },
  {
    name: "Tajada Ortiz Urbina, Lourdes",
    area: "D.RRHH.España"
  },
  {
    name: "Tamayo Tomás, Sara",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Tapia Tovar, José Antonio",
    area: "Comercial Educación Delegación Andalucía Extremadura"
  },
  {
    name: "Tauroni Muñoz, Aránzazu",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Tejedor Ruiz, Manuel",
    area: "Dirección Sistemas España"
  },
  {
    name: "Tellechea Mora, Teresa Magdalena",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Tena Pérez, Endika",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Tijero Calvo, María Paz",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Tolsada Peris, Diego",
    area: "Edición Religiosa"
  },
  {
    name: "Tornero Herrero, Enrique",
    area: "Comercial Educación Delegación Madrid"
  },
  {
    name: "Toro Sánchez, Francisco Miguel",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Torres García, Iria",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Treviño Ortiz, Rafael",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Valerio Brotons, Elena",
    area: "Literatura Infantil y Juvenil"
  },
  {
    name: "Valero Díaz, Francisco",
    area: "Logística"
  },
  {
    name: "Vallejo Aroca, Cecilia",
    area: "Marketing España"
  },
  {
    name: "Vallina Fernández Montes, Nuria",
    area: "Dir. Contenidos y Servicios Educativos España"
  },
  {
    name: "Valls Catalá, Adrián",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Varona González, Álvaro",
    area: "Dirección Corporativa Servicios y Productos"
  },
  {
    name: "Vela García, Esperanza",
    area: "Edición Religiosa"
  },
  {
    name: "Ventura Tadeo, Pedro",
    area: "Comercial Educación Delegación Canarias"
  },
  {
    name: "Vercher Navarro, Miguel",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Vilar Álvarez, Sonia",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Villa Díez, Gustavo",
    area: "Dirección Desarrollo Tecnológico"
  },
  {
    name: "Villamediana Caballero, José Antonio",
    area: "Comercial Educación  Delegación Norte"
  },
  {
    name: "Villelga Rodríguez, David Nicolás",
    area: "Dirección Sistemas España"
  },
  {
    name: "Viñado Fogué, José Antonio",
    area: "Comercial Educación Delegación Norte"
  },
  {
    name: "Viñas Álvaro, Antonio",
    area: "Ventas Literatura Infantil y Juvenil"
  },
  {
    name: "Yáñez Yáñez, Silvia",
    area: "Dirección Producto Tecnológico"
  },
  {
    name: "Zahínos Hinchado, Elena",
    area: "Logística"
  },
  {
    name: "Zamorano Tapia, Sebastián",
    area: "Dirección Sistemas España"
  },
  {
    name: "Zazo Fernández, María del Rosario",
    area: "Dir. Contenidos y Servicios Educativos España"
  }
];
