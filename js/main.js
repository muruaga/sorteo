
function Sorteo (canvasDOM, cx, cy, radius ) {

	var canvas = canvasDOM,
		ctx = null,
		sAngle = 0,
		sectionAngle = 360 / 5,
		spinTimeout = null, // stores the identifier of the function setTimeout
		spinAngleStart = 10,
		spinTime = 0,
		spinTimeTotal = 0,
		$dlgText = null,
    $dlgTextContainer = null,
    $dlgTextBtnClose = null,
    $dlgWinners = null,
    $dlgWinnersContent = null;

  window.people = [];
  window.namesIndex = [];
  var timeNames = null;


	// checking if browser supports canvas
	if (typeof canvas.getContext === "function") {
		ctx = canvas.getContext("2d");
	} else {
		ctx = null;
		throw new CanvasUnsupportedException();
	}


	var constants = {
		outsideRadius : radius,
		rad : Math.PI / 180,
		center : {
			x : cx,
			y : cy
		},
		dimensions : {
			h : canvasDOM.height,
			w : canvasDOM.width
		},
    colors: ['#6edaf7', '#c6de70', '#c792ca', '#ffd049', '#ed0a28']
	};


	this.init = function() {
		// Elementos de la popup del ganador.
		$dlgText = $('.dlg-text');
    $dlgTextContainer = $dlgText.find('.container');
    $dlgTextBtnClose =  $dlgText.find('.btn-close');
    $dlgWinners = $('.dlg-winners');
    $dlgWinnersContent = $dlgWinners.find('.container .content');

    // Close pupup
    $('.btn-close').on('mousedown touchstart', function (event) {
      event.preventDefault();
      event.stopPropagation();
      hideDialogs();
    });

    // Seleccionar los primeros 5 nombres.
    selectRandomNames(0, 0, function(tmpNames, tmpNamesIndex) {
      people = tmpNames;
      namesIndex = tmpNamesIndex;
    	repaint();
    });
	};


  /**
	 * Desordena un array aleatoriamente.
   * @param array
   * @returns {*}
   */
  var shuffle = function (array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  };


  /**
	 * Repinta la ruleta con laz zonas y nombre.
   */
	var repaint = function() {
		ctx.clearRect(0, 0, constants.dimensions.width, constants.dimensions.height);
		ctx.strokeStyle = "black";
		ctx.lineWidth = 1;

		for (var i = 0, ii = 5; i < ii; i++) {
			var eAngle = sAngle + sectionAngle;

			ctx.fillStyle = constants.colors[i];
			ctx.beginPath();
			// syntax: context.arc(cx, cy,  radiusOftheCircle, startingAngleInRadians, endingAngleInRadians, counterclockwise);
			ctx.arc(constants.center.x, constants.center.y, 
			constants.outsideRadius, converter.degreesTOradians(sAngle),
			converter.degreesTOradians(eAngle));
			ctx.lineTo(constants.center.x, constants.center.y);
			ctx.closePath();
			ctx.stroke(); // draws the path
			ctx.fill(); // Fills the current drawing

			addName(i, sAngle, eAngle);

			sAngle = eAngle;
		}
	};


  /**
	 * Devuelve un número entre un mínimo y máximo
   * @param min
   * @param max
   * @returns {*}
   */
  var getRandomInt = function (min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  };


  /**
	 * Seleccina entre lo nombres de la lista 5
   * @param min
   * @param max
   * @param callback
   */
	var selectRandomNames = function (min, max, callback) {
    clearTimeout(timeNames);
    var sec = getRandomInt(min, max);
    timeNames = setTimeout(function () {
      clearTimeout(timeNames);

      var tmpNames = [];
      var tmpNamesIndex = [];
      while (tmpNames.length < 5) {
        var index = getRandomInt(0, data.length);
        var person = data[index];
        if (tmpNames.indexOf(person.name) === -1) {
          tmpNames.push(person.name);
          tmpNamesIndex.push(index);
        }
      }
      callback(tmpNames, tmpNamesIndex);
    }, sec * 1000);
  };


  /**
   * Devuel los datos de la persona en nombre y apellidos.
   * @param text
   * @returns {{firstName: (string|*), lastName: (string|*)}}
   */
	var parseName = function (person, ellipsis) {
    var arrname = person.split(',');
    var fname = arrname[1].trim().replace('-', ' ');
    var lname = arrname[0].trim().replace('-', ' ')

    if (ellipsis) {
    	if (fname.length > 20) {
        fname = fname.substring(0, 17) + '...'
			}
      if (lname.length > 20) {
        lname = lname.substring(0, 17) + '...'
      }
		}

    return {
      firstName : fname,
      lastName : lname
    };
  };


  /**
	 * Adiciona un nombre a la zona ruleta
   * @param index
   * @param sAngle
   * @param eAngle
   */
	var addName = function(index, sAngle, eAngle) {
		var points = getPoints(sAngle, eAngle),
			width = 60,
			height = 70,
			size = 1,
			//Convert degrees to radian
			radians = converter.degreesTOradians(sAngle += sectionAngle*1.7);

			//save the context
			ctx.save();
			
			//Set the origin to the center of the image
			ctx.translate(points.x1, points.y1);

			//Rotate the canvas around the origin
			ctx.rotate(radians);

			ctx.font = "20px Arial";
			ctx.fillStyle = "black";
			ctx.textAlign = "center";

      var person = parseName(people[index], true);
    	ctx.fillText(person.firstName, width / size * ( -1 ), height / size * ( -1 ));
      width -= 20;
      height -= 20;
      ctx.fillText(person.lastName, width / size * ( -1 ), height / size * ( -1 ));

			//reset the canvas
			ctx.rotate(radians * ( -1 ) );
			ctx.translate(points.x1 * ( -1 ), points.y1 * (-1));

			//restore the context to its original state
			ctx.restore();
	};


  /**
   * Actualiza los nombres en la ruleta de forma aleatoria.
   */
	var refreshNames = function () {
		selectRandomNames(1, 3, function (tmpNames, tmpNamesIndex) {
			if (isSpin) {
        people = tmpNames;
        namesIndex = tmpNamesIndex;
				repaint();
				refreshNames();
			}
		});
  };


  var isSpin = false; // Indica si se está en es momento animando la rulta.

  /**
   * Inicia la animación de la ruleta.
   */
	this.spin = function() {
	  if (!isSpin) {
      // Desordenado de la lista de participantes
      data = shuffle(data);

      isSpin = true;
      spinAngleStart = Math.random() * 10 + 10;
      spinTime = 0;
      spinTimeTotal = Math.random() * 3 + getRandomInt(10000, 15000);
      this.rotateWheel();

      refreshNames();
    }
	};


  /**
   * Gira la ruleta
   */
	this.rotateWheel = function() {
		spinTime += 30;
		if (spinTime >= spinTimeTotal) {
			stopRotateWheel();
			return;
		}
		var spinAngle = spinAngleStart - easeOut(spinTime, 0, spinAngleStart, spinTimeTotal);
		sAngle += spinAngle;
		repaint();
		spinTimeout = setTimeout('sorteo.rotateWheel()', 30);
	};


  /**
   * Detiene la animacioón de la ruleta.
    */
  var stopRotateWheel = function() {
    isSpin = false;
		spinTimeout = clearTimeout(spinTimeout);
		var index = Math.floor((360 - (sAngle+90) % 360) / sectionAngle);
    showDialogWinner(index);
	};


  /**
   * Muestra el dialo con la perona ganadora.
   * @param index
   */
	var showDialogWinner = function(index) {
    hideDialogs();

    var person = data[namesIndex[index]];
    var personName = parseName(person.name, false);
    var personArea = person.area;
    data.splice(namesIndex[index], 1);

    $dlgText.find('.text').html(personName.firstName + '<br>' + personName.lastName);
    $dlgText.find('.area').html(personArea);
    $dlgTextContainer.css('border-color', constants.colors[index]);
    $dlgTextBtnClose.css('background-color', constants.colors[index]);
    $dlgText.removeClass('hidden');
    $dlgWinnersContent.append(
    	'<li class="person">' +
			'<div class="text">' + personName.firstName + ' ' + personName.lastName + ' </div>' +
      '<div class="area">' + personArea + '</div>' +
			'</li>'
		);
	};


  this.showDialogAllWinner = function() {
    hideDialogs();
    $dlgWinners.removeClass('hidden');
  };


  /**
   * Cieraa el dialogo de la persona.
   */
  var hideDialogs = function () {
    $dlgText.addClass('hidden');
    $dlgWinners.addClass('hidden');
  };



	var easeOut = function(t, b, c, d) {
		var ts = (t /= d) * t;
		var tc = ts * t;
		return b + c * (tc + -3 * ts + 3 * t);
	};

	// calculates the points at which the arc begins and ends
	var getPoints = function (sAngle, eAngle) {
		sAngle += sectionAngle/1.3;
		var points = {
			x1 : cx + constants.outsideRadius/1.9 * Math.cos(sAngle * constants.rad),
			x2 : cx + constants.outsideRadius * Math.cos(eAngle * constants.rad),
			y1 : cy + constants.outsideRadius/1.9 * Math.sin(sAngle * constants.rad),
			y2 : cy + constants.outsideRadius * Math.sin(eAngle * constants.rad)
      };
    return points;
	};


	// converter from degrees to radians and vice versa
	var converter = {
		degreesTOradians : function(degrees) {
			return degrees*Math.PI/180;
		},
		radiansTOdegrees : function(radians) {
			return (radians*180)/Math.PI;
		}
	};

	// Object exception is thrown if canvas is not supported
	function CanvasUnsupportedException () {
		this.message = "You are using an browser without support to use HTML5 Canvas";
		this.toString = function() {
			return this.message
		};
		alert("Usted está utilizando un navegador sin soporte para uso de HTML5 Canvas");
	}
}


$(function() {

	window.sorteo = new Sorteo(
		document.getElementById('wheelcanvas'),
		271,
		287,
		230
	);


	$(document).ready(function () {

    sorteo.init();

  });

});